# README - jobtech-jobad-enrichments-tf-serving

## Build
### Master:  
podman build -t jobadenrichmentstfserving:latest .


## Run
### REST:
podman run -d -p 8501:8501 --name jobadenrichmentstfserving jobadenrichmentstfserving

### REST with local network to reach TF serving in other local podman image:
podman run -d -p 8501:8501 --name jobadenrichmentstfserving --network docker_opensearch_local jobadenrichmentstfserving

### gRPC:
podman run -d -p 8500:8500 --name jobadenrichmentstfserving jobadenrichmentstfserving

## Check log file
podman logs jobadenrichmentstfserving --details -f

## Stop & remove image
podman stop jobadenrichmentstfserving;podman rm jobadenrichmentstfserving || true


## Debug podman
podman logs jobadenrichmentstfserving --details -f
podman exec -t -i jobadenrichmentstfserving /bin/bash
podman run -it --rm jobadenrichmentstfserving:latest

