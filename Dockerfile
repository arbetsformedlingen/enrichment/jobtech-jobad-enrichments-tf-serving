FROM tensorflow/serving:2.13.0

# Se https://hub.docker.com/r/tensorflow/serving/tags/

# See: https://github.com/tensorflow/serving/blob/master/tensorflow_serving/g3doc/building_with_docker.md
# https://github.com/tensorflow/serving/blob/master/tensorflow_serving/tools/docker/Dockerfile
# Batching: https://github.com/tensorflow/serving/blob/master/tensorflow_serving/batching/README.md

#ADD https://github.com/dotcloud/docker/archive/master.tar.gz /tmp/
#ADD https://text-enrichments-resourc-textdoc-enrichments-resources.dev.services.jtech.se/resources/wordembedding_models/47f10663/fasttext_model_gensim_narval_cleaned2 /models/

#RUN apt-get update && apt-get install -y \
#        wget
#RUN wget --no-parent -r https://text-enrichments-resourc-textdoc-enrichments-resources.dev.services.jtech.se/resources/wordembedding_models/47f10663 -P /models/

COPY resources/prediction_models_tf_keras/*.config /models/
# TODO: Mount drive in Openshift and copy files automatically from S3/Amazon.
COPY resources/prediction_models_tf_keras/models_no_lfs/ /models/

ENV MODEL_CONFIG_FILE=/models/models.config

# Port 8500 for gRPC
#EXPOSE 8500
EXPOSE 8501


# Create a script that runs the model server so we can use environment variables
# while also passing in arguments from the docker command line
RUN echo '#!/bin/bash \n\n\
tensorflow_model_server --rest_api_port=8501 --model_config_file=${MODEL_CONFIG_FILE} \
"$@"' > /usr/bin/tf_serving_entrypoint.sh \
&& chmod +x /usr/bin/tf_serving_entrypoint.sh

USER 10001

ENTRYPOINT ["/usr/bin/tf_serving_entrypoint.sh"]
