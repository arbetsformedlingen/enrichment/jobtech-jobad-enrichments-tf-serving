# Textdoc enrichment api Openshift setup
* För att köra så behövs Openshift cli-tools vara installerade på maskinen du kör 
scriptet från.
* Logga sedan in via Openshift web consolen och kopiera loginkommandot.
* Logga därefter in med commandot via en terminal.
* Stå i roten av repot.
* En configmap propertiesfil som sätter environmentvariabler i api-poden
behöver skapas innan du kan köra scriptet.  
Detta kan göras från terminalen med följande kommando (obs fyll i lösenord etc):
~~~
cat > ./config/configMapTextdocEnrichment.properties <<EOF
ES_HOST=3d1803df463441cfb7759de0d6573943.eu-west-1.aws.found.io
ES_PORT=9243
ES_USER=elastic
ES_PWD=
ES_ONTOLOGY_HOST=https://3d1803df463441cfb7759de0d6573943.eu-west-1.aws.found.io:9243
ES_ONTOLOGY_USER=elastic
ES_ONTOLOGY_PWD=
EOF
~~~
* Om du vill ändra några inställningar (gitrepo, namn, projektnamn etc) så finns alla dessa
som variabler i setup_jobtech_jobad_enrichments_tf_serving_test scriptet.

Openshift 4 (Test, Git-branch: master)
------------ 
* Kör script enligt:
~~~
sh setup_jobtech_jobad_enrichments_tf_serving_dev.sh
sh setup_jobtech_jobad_enrichments_tf_serving_test.sh
sh setup_jobtech_jobad_enrichments_tf_serving_prod.sh
~~~
För internt API (Platsbanken):
~~~
sh setup_jobtech_jobad_enrichments_tf_serving_internal_test.sh
sh setup_jobtech_jobad_enrichments_tf_serving_internal_prod.sh
~~~
Delete project in Openshift
--------------------------------
~~~
oc delete project jobad-enrichments-tf-serving-dev
oc delete project jobad-enrichments-tf-serving-test
oc delete project jobad-enrichments-tf-serving-prod

oc delete project jobad-enrichments-tf-serving-internal-test
oc delete project jobad-enrichments-tf-serving-internal-prod


~~~