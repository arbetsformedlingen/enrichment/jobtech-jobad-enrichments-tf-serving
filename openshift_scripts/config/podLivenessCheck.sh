#!/bin/bash
echo ">>> LIVENESS CHECK FOR POD ${1} TO PROJECT ${2}"
sleep 5
while : ; do
  echo ">>> CHECK IF POD: ${1} IS ALIVE."
  oc get pod -n $2 | grep $1 | grep -v build | grep -v deploy |grep "1/1.*Running"
  [[ "$?" == "1" ]] || break
  echo "<<< NOT YET :( >>>>> WAITING 1O MORE SECONDS AND TRY AGAIN."
  sleep 5
done
